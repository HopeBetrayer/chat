# Chat app

# Description

Socket chat app
___

# Used technologies
## Backend
* Koa
* Redis
* Socket.io
* mongodb
* node.js 8.9

## Frontend
* react
* socket.io client
___
# Requirements
* Node.js 8.9
* redis server (running on 127.0.0.1:6379 by default)
* mongodb (running on 127.0.0.1:27017 by default)
___
# Installation

1. clone git repository and `cd chat`
2. `npm install` ( notice that npm runs 'postinstall' script. If postinstall script fails - run `cd frontend && npm install` )
3. Check config and frontend/src/config folder and adjust environmental configuration
4. `mkdir public` or create public dir at the root of the project
5. `npm run frontend:deploy` 
6. `npm start`
___
# Example
[ here ](http://its.bet:3000)

