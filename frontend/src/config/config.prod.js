module.exports = {
  host: 'http://localhost',
  port: 3000,
  fetchParams: {
    credentials: "include",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    mode: 'cors'
  },
  apiURL: '',
}
