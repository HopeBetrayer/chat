import React, { Component } from 'react';

import API from './services/api';
import ChatContainer from './components/ChatContainer';
import LoginScreen from './components/LoginScreen';
import './App.css';

class App extends Component {
  state = {
    loggedIn: false,
    initialized: false,
  }

  componentDidMount = async () => {
    const response = await API.get('/auth');
    const responseData = await response.json();
    this.handleLogin(responseData);
  }

  handleLogin = response => {
    const { status } = response;
    const success = status === 'success';
    if (success) {
      const { payload } = response;
      console.log(payload, response);
      this.setState({ loggedIn: success, username: payload.username  });
    } else {
      this.setState({ loggedIn: success, username: null  });
    }
  }

  handleLogout = async () => {
    await API.delete('/auth');
    this.setState({ loggedIn: false, username: null });
  }
  render() {
    const { loggedIn, username } = this.state;
    return (loggedIn ? <ChatContainer user={ username } onLogout={ this.handleLogout } /> : <LoginScreen onLogin={ this.handleLogin } />)
  }
}

export default App;
