import config from '../config';

class API {
  static get(url) {
    const requestParams = {
      method: 'GET',
      ...config.fetchParams
    };
    return fetch(url, requestParams);
  }
  static post(url, data) {
    const requestParams = {
      method: 'POST',
      body: JSON.stringify(data),
      ...config.fetchParams
    };
    return fetch(url, requestParams);
  }
  static delete(url) {
    const requestParams = {
      method: 'DELETE',
      ...config.fetchParams
    };
    return fetch(url, requestParams);
  }
  static put(url, data) {
    const requestParams = {
      method: 'PUT',
      body: JSON.stringify(data),
      ...config.fetchParams
    };
    return fetch(url, requestParams);
  }
}

export default API;
