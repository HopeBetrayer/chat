import React, { Component } from 'react';
import './styles.css';

class ChatMessage extends Component {
  renderMessage = message => {
    const { myMessage } = this.props;
    return (
      <div className={ `message__container ${ message.target ? 'private-message' : '' } ${ myMessage ? 'my-message' : '' }` }>
      <span className="message__username"> { message.username }: </span>
      <span className="message__text"> { `${message.message}` } </span>
      </div>
    )
  }
  renderAnnounce = message => (
    <div className="announce__container">
      <span className="announce__text"> { message.message } </span>
    </div>
  )
  render() {
    const { message } = this.props;
    return message.announce ? this.renderAnnounce(message) : this.renderMessage(message);
  }
}

export default ChatMessage;
