import React, { Component } from 'react';

import './styles.css';

class Registration extends Component {
  state = {
    username: '',
    password: '',
    error: null,
  }
  register = () => {
    const { onRegister } = this.props;
    onRegister && onRegister(this.state);
  }
  handleInput = name => {
    return e => {
      const newState = {};
      newState[name] = e.target.value;
      this.setState(newState);
    }
  }
  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.register();
    }
  }
  render() {
    return (
        <div className="registration__panel">
          <h2 className="registration__title"> Registration </h2>
          <input
            type="text"
            className="registration__input"
            placeholder="Username"
            value={ this.state.username }
            onChange={ this.handleInput('username') }
            onKeyPress={ this.handleKeyPress }
            />
          <input
            type="password"
            className="registration__input"
            placeholder="Password"
            value={ this.state.password }
            onChange={ this.handleInput('password') }
            onKeyPress={ this.handleKeyPress }
            />
          <button
            className="registration__button"
            onClick={ this.register }
            >
            Register
          </button>
        </div>
    );
  }
}

export default Registration;
