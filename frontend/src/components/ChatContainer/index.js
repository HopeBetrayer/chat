import React, { Component } from 'react';
import ChatWindow from '../ChatWindow';
import MessageInput from '../MessageInput';
import Socket from '../Socket';
import './styles.css';

class ChatContainer extends Component {
  state = {
    socket: null,
  }
  setSocket = socket => {
    if (this.props.socket) {

    }
    if (socket) {

      this.setState({ socket });
    }
  }
  render() {
    const { socket } = this.state;
    const { user } = this.props;
    return (
      <div>
        <Socket onSocketChange={ this.setSocket } />
        <ChatWindow user={ user } onLogout={ this.props.onLogout } key="chat" socket={ socket } />
        <MessageInput key="message" socket={ socket } />
      </div>
    );
  }
}

export default ChatContainer;
