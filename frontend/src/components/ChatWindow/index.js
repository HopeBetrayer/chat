import React, { Component } from 'react';
import _ from 'lodash';
import ChatMessage from '../ChatMessage';
import './styles.css';

class ChatWindow extends Component {
  state = {
    messages: [],
    typing: [],
    onlineUsers: [],
  }
  showTyping = data => {
    const { username } = data;
    const { typing } = this.state;
    if (typing.indexOf(username) === -1) {
      typing.push(username);
    }
    this.setState({ typing });
    this.scroll();
  }
  hideTyping = data => {
    const { username } = data;
    const { typing } = this.state;
    const reducedTyping = typing.filter(name => name !== username);
    this.setState({ typing: reducedTyping });
  }
  showMessage = message => {
    const { messages } = this.state;
    const newMessages = messages.concat(message);
    this.setState({ messages: newMessages });
    this.scroll();
  }
  showAnnounce = message => {
    const { messages } = this.state;
    messages.push({
      announce: true,
      message
    });
    this.setState({ messages });
    this.scroll();
  }

  updateOnlineUsers = users => {
    this.setState({ onlineUsers: users });
  }
  scroll = () => {
    const element = this.refs.scroller;
    if (element) {
      element.scrollTop = element.scrollHeight;
    }
  }
  setListeners = socket => {
    socket.on('update online users', this.updateOnlineUsers);
    socket.on('new message', this.showMessage);
    socket.on('private message', this.showMessage);
    socket.on('announce', this.showAnnounce);
    socket.on('typing', this.showTyping);
    socket.on('stopped typing', this.hideTyping);
  }
  clearListeners = socket => {
    socket.off('update online users');
    socket.off('new message');
    socket.off('private message');
    socket.off('announce');
    socket.off('typing');
    socket.off('stopped typing');
  }
  componentDidMount = () => {
    const { socket } = this.props;
    if (socket) {
      this.setListeners(socket);
    }
  }

  componentWillReceiveProps = newProps => {
    const { socket } = this.props;
    if (socket) {
      this.clearListeners(socket);
    }
    if (newProps.socket) {
      this.setListeners(newProps.socket)
    }
  }

  componentWillUnmount = () => {
    const { socket } = this.props;
    this.clearListeners(socket);
  }
  logout = () => {
    const { onLogout } = this.props;
    onLogout && onLogout();
  }
  renderUser = (username, index) => {
    const { user } = this.props;
    return (
      <div key={ index } className={ `user-list__user ${user.toLowerCase() === username.toLowerCase() ? 'user-list__me' : ''}` }>
        <span> { username } </span>
      </div>
    )
  }

  render() {
    const { onlineUsers, messages, typing } = this.state;
    const { user } = this.props;
    return (
      <div className="chat">
        <div className="scroller-container">
          <div ref="scroller" className="scroller">
            { messages.map((msg, index) => <ChatMessage myMessage={ (msg.username || '').toLowerCase() === user.toLowerCase() } key={ index } message={ msg } />) }
            <span className="status-message"> { typing.length ? `${typing.join(', ')} typing...` : ''  } </span>
          </div>
        </div>
        <div className="user-list">
        <button className="logout-button" onClick={ this.logout }> Logout </button>
        <h3 className="user-list__title"> Online users: </h3>
        { onlineUsers.map(this.renderUser) }
        </div>
      </div>
    );
  }
}

export default ChatWindow;
