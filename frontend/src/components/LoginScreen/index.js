import React, { Component } from 'react';

import API from '../../services/api';
import Login from '../Login';
import Registration from '../Registration';

import './styles.css';

class LoginScreen extends Component {
  state = {
    login: true,
    error: null,
  }
  toggle = () => {
    const { login } = this.state;
    this.setState({ login: !login, error: null });
  }

  onRegister = async (data) => {
    const { username, password } = data;
    const { onLogin } = this.props;
    this.setState({ error: null })
    const response = await API.post('/auth/register', data);
    const responseData = await response.json();
    if (responseData.errorMessage) {
        this.setState({ error: responseData.errorMessage });
    }
    onLogin && onLogin(responseData);
  }

  onLogin = async (data) => {
    const { username, password } = data;
    const { onLogin } = this.props;
    const response = await API.post('/auth', data);
    const responseData = await response.json();
    if (responseData.errorMessage) {
      this.setState({ error: responseData.errorMessage });
    }
    onLogin && onLogin(responseData);
  }
  render() {
    const { login, error } = this.state
    return (
      <div className="overlay">
        <div className="container">
          { login ? <Login onLogin={ this.onLogin } /> : <Registration onRegister={ this.onRegister } /> }
          { error ? <span className="error"> { error } </span> : null}
          <span className="switcher" onClick={ this.toggle }>
            { login ? 'No account? Register! ': 'Already have an account? Log In!' }
          </span>
        </div>
      </div>
    );
  }
}

export default LoginScreen;
