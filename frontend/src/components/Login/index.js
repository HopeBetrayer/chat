import React, { Component } from 'react';

import './styles.css';

class Login extends Component {
  state = {
    username: '',
    password: '',
    error: null,
  }
  login = () => {
    const { onLogin } = this.props;
    onLogin && onLogin(this.state);
  }
  handleInput = name => {
    return e => {
      const newState = {};
      newState[name] = e.target.value;
      this.setState(newState);
    }
  }
  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.login();
    }
  }
  render() {
    return (
      <div className="login__panel">
        <h2 className="login__title"> Log In </h2>
        <input
          type="text"
          className="login__input"
          placeholder="Username"
          value={ this.state.username }
          onChange={ this.handleInput('username') }
          onKeyPress={ this.handleKeyPress }
          />
        <input
          type="password"
          className="login__input"
          placeholder="Password"
          value={ this.state.password }
          onChange={ this.handleInput('password') }
          onKeyPress={ this.handleKeyPress }
          />
        <button
          className="login__button"
          onClick={ this.login }
          >
          Join
        </button>
      </div>
    );
  }
}

export default Login;
