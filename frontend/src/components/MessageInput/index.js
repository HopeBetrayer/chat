import React, { Component } from 'react';
import _ from 'lodash';

import './styles.css';

class MessageInput extends Component {
  state = {
    message: '',
  }
  stopTyping = _.debounce(() => {
    const { socket } = this.props;
    socket && socket.emit('stopped typing');
  }, 1000)

  startTyping = _.throttle(() => {
    const { socket } = this.props;
    socket && socket.emit('typing');
  }, 300)

  onChange = e => {
    const message = e.target.value;
    const { socket } = this.props;
    this.startTyping();
    this.stopTyping();
    this.setState({ message });
  }
  sendMessage = e => {
    const { socket } = this.props;
    const text = this.state.message;
    const textContent = text.split(' ');
    const nickExp = /^\@[\w\d]+/ig;
    const isPrivate = textContent[0] && nickExp.test(textContent[0]);
    if (isPrivate) {
      const username = textContent[0];
      socket.emit('private message', {
        target: username.substr(1),
        message: text
      });
    } else {
      socket.emit('message', text);
    }
    this.setState({ message: ''});
  }
  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.sendMessage();
    }
  }
  render() {
    return (
      <div className="message-input__container">
        <input
          type="text"
          className="message-input"
          placeholder="Enter message here. Use @nickname to send private message"
          value={ this.state.message }
          onChange={ this.onChange }
          onKeyPress={ this.handleKeyPress }
          />
        <button
          className="message-input__send-button"
          onClick={ this.sendMessage }
          >
          Send
        </button>
      </div>
    );
  }
}

export default MessageInput;
