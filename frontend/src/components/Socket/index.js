import { Component } from 'react';
import IO from 'socket.io-client';
import config from '../../config';

class Socket extends Component {
  state = {
    socket: null
  }
  componentDidMount = () => {
    const socket = IO(`${config.host}:${config.port}`);
    const { onSocketChange } = this.props;
    this.setState({ socket });
    onSocketChange && onSocketChange(socket);
  }
  render() {
    return null;
  }
}

export default Socket;
