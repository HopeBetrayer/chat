const Message = require('../models/message');

class Messager {
  constructor() {
    this.users = {}
  }
  addUser(socket, username) {
    this.users[username.toLowerCase()] = socket;
    socket.on('message', ctx => this.onMessage(ctx));
    socket.on('private message', ctx => this.onPrivateMessage(ctx))
  }
  removeUser(username) {
    delete this.users[username.toLowerCase()];
  }
  async onMessage(ctx) {
    const { data, socket, session } = ctx;
    const { username } = session.user;
    const newMessage = await this.saveMessage({
      target: null,
      message:data,
      date: new Date(),
      username: username.toLowerCase(),
    });
    socket.broadcast('new message', newMessage);
  }
  async onPrivateMessage(ctx) {
    const { data, session } = ctx;
    const { username } = session.user;
    const { target, message } = data;
    const socket = this.users[target.toLowerCase()];
    if (socket) {
      if (target.toLowerCase() === username.toLowerCase()) {
          ctx.socket.emit('announce', `You can't send private messages to yourself`);
          return;
      }
      const newMessage = await this.saveMessage({
        target: target.toLowerCase(),
        message,
        date: new Date(),
        username: username.toLowerCase(),
      });
      socket.emit('new message', newMessage);
      ctx.socket.emit('new message', newMessage);
    } else {
      ctx.socket.emit('announce', `There is no user ${target} in chat`);
    }
  }
  async saveMessage({ username, date, message, target }) {
    const newMessage = new Message({
      message,
      date,
      username,
      target,
    });
    await newMessage.save();
    return newMessage;
  }
  async getLastMessages(username, count = 10) {
     return await Message.find({
       $or: [
         { target: null },
         { target: username.toLowerCase() },
         { username: username.toLowerCase() },
       ]
     }).sort('-date')
     .select('-__v -_id')
     .limit(10)
     .exec();
  }
}
module.exports = Messager;
