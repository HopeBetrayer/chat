const IO = require('koa-socket.io');
const koaSocketSession = require('koa-socket-session');

const OnlineUsersHelper = require('../util/OnlineUsers');
const Messager = require('./messager');
const config = require('../../config');

const chat = new IO(config.chat);
const OnlineUsers = new OnlineUsersHelper();
const messager = new Messager();

const onJoin = async (ctx) => {
  const { socket, data, session } = ctx;
  const lastMessages = await messager.getLastMessages(session.username);
  await OnlineUsers.addUser(data);
  const users = await OnlineUsers.getUsers();
  lastMessages.reverse();
  socket.emit('new message', lastMessages);
  socket.emit('joined', true);
  socket.broadcast('announce', `User ${data} joined the chat`);
  socket.broadcast('update online users', users);
};

const onTyping = async (ctx) => {
  const { socket, session } = ctx;
  socket.broadcast('typing', {
    username: session.username,
  });
};

const onStopTyping = async (ctx) => {
  const { socket, session } = ctx;
  socket.broadcast('stopped typing', {
    username: session.username,
  });
};

const onDisconnect = async (ctx) => {
  const { socket, data, session } = ctx;
  await OnlineUsers.removeUser(session.username);
  const users = await OnlineUsers.getUsers();
  socket.broadcast('update online users', users);
  socket.broadcast('announce', `User ${session.username} has left the chat`);
};
const onConnection = async (ctx, next) => {
  const { socket, session } = ctx;

  if (!session.username) {
    socket.disconnect();
    return false;
  }

  onJoin({ socket, data: session.username, session});
  messager.addUser(socket, session.username);
  socket.on('join', onJoin);
  socket.on('typing', onTyping);
  socket.on('stopped typing', onStopTyping);
  socket.on('disconnect', onDisconnect);
}

chat.init = ({app, session, server}) => {
  try {
    chat.start(server, config.wsServer);
    chat.use(koaSocketSession(app, session));
    chat.on('connect', onConnection);
  } catch (e) {
    console.error(e);
  }
}

module.exports = chat;
