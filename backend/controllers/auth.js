const User = require('../models/user');
const ResponseBuilder = require('../util/ResponseBuilder');

class AuthController {
  static async register(ctx) {
    const data = ctx.request.body;
    const { session } = ctx;
    data.username = data.username.toLowerCase();
    const existingUser = await User.findOne({ username: data.username });
    if (existingUser) {
      ResponseBuilder.error(ctx, 400, 'This username is already taken');
      return true;
    }
    const user = new User(data);
    const errors = user.validateSync();
    try {
      await user.save();
      session.username = data.username;
      session.user = user;
      ResponseBuilder.success(ctx, { id: user._id, username: user.username });
    }
    catch (e) {
      ResponseBuilder.error(ctx, 400, 'Please provide login and password');
    }
  }

  static async login(ctx) {
    const { username, password } = ctx.request.body;
    const { session } = ctx;
    const user = await User.findOne({ username });
    if (user && user.password === password) {
      session.username = username;
      session.user = user;
      ResponseBuilder.success(ctx, { id: user._id, username: user.username });
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid login or password');
    }
  }

  static async checkAuth(ctx) {
    const { session } = ctx;
    if (session.username && session.user) {
      const { user } = session;
      ResponseBuilder.success(ctx, { id: user._id, username: user.username });
    } else {
      ResponseBuilder.error(ctx, 401, 'unauthorized');
    }
  }

  static async logout(ctx) {
    ctx.session = null;
  }
}
module.exports = AuthController;
