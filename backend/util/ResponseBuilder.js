class ResponseBuilder {
  static success(ctx, data) {
    const response = {
      status: 'success',
      payload: data,
      timestamp: new Date().getTime()
    };
    ctx.body = JSON.stringify(response);
  }
  static error(ctx, status, message) {
    const response = {
      status: 'error',
      errorCode: status,
      errorMessage: message,
      timestamp: new Date().getTime()
    };
    ctx.status = status;
    ctx.body = JSON.stringify(response);
  }
}

module.exports = ResponseBuilder;
