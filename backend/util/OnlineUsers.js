const redis = require('redis');
const config = require('../../config');

class OnlineUsers {
  constructor() {
    this.redisClient = redis.createClient(config.redis);
    this.users = [];
  }
  async addUser(username) {
    let users = await this.getUsers();
    if (users.indexOf(username) === -1) {
      users.push(username);
    }
    await this.setUsers(users);
  }
  async setUsers(data = null) {
    const { redisClient, users } = this;
    redisClient.set(config.onlineUsersKey, JSON.stringify(data || this.users));
  }
  async removeUser(username) {
    let users = await this.getUsers();
    users = users.filter(name => name.toLowerCase() !== username.toLowerCase());
    await this.setUsers(users);
  }
  getUsers() {
    const { redisClient } = this;
    return new Promise((resolve, reject) => {
      redisClient.get(config.onlineUsersKey, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(JSON.parse(data) || [])
        }
      });
    })
  }
}

module.exports = OnlineUsers;
