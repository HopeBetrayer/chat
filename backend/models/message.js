const mongoose = require('mongoose');
const schema = new mongoose.Schema({
  message: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  target: {
    type: String,
  }
});

module.exports = mongoose.model('Message', schema);
