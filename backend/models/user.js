const mongoose = require('mongoose');
const schema = new mongoose.Schema({
  username: {
    type: String,
    required: "Username required",
    unique: "Username is already taken",
  },
  password: {
    type: String,
    required: "Password required",
    validate: {
      validator: value => !/[^0-9A-Za-z\-\_\!\@\#\$]/i.test(value),
      message: "Password contains invalid characters",
    },
    min: [6, 'Password is too short'],
    max: [20, 'Password is too long'],
  },
  status: {
    type: Boolean
  }
});

module.exports = mongoose.model('User', schema);
