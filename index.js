const Koa = require('koa');
const KoaSession = require('koa-session');
const redisStore = require('koa-redis');
const serve = require('koa-static');
const bodyParser = require('koa-bodyparser');
const compose = require('koa-compose');
const http = require('http');
const mongoose = require('koa-mongoose');

const authRouter = require('./backend/routers/auth');
const chat = require('./backend/controllers/chat');
const config = require('./config');

const app = new Koa();

const routes = compose([
  authRouter.routes(),
]);

app.keys = config.appKeys;

const sessStore = redisStore(config.redis);
const sessionConfig = {
  store: sessStore,
  ...config.store
};

const session = KoaSession(sessionConfig, app);
app.use(mongoose({
    mongoose: require('mongoose-q')(),
    ...config.mongoose
}))
app.use(session);
app.use(bodyParser());
app.use(routes);
app.use((ctx, next) => {
  if (ctx.path.indexOf('/auth') != 0) {
    return serve('./public')(ctx, next);
  }
});

const server = http.Server(app.callback());

chat.init({ app, server, session });

server.listen(config.port);
